#ifndef _MOTOR_H_
#define _MOTOR_H_
#include "STC8.h"
#include <intrins.h>
#include "key.h"
#include "uart.h"	
#include "delay.h"	
#define Forward 1   //正
#define Back    0		//反
sbit A_PH=P1^0; //????A???	
sbit B_PH=P1^1; //????B???
sbit C_PH=P1^2; //????C???
sbit D_PH=P1^3; //????D???


extern unsigned int run_num;
void Delay5ms(); //延时
void Delay1ms();
void Delay500us();	

void motor_run_clockwise(); //正转
void motor_run_counterclockwise(); //反转
void motor_run_counter_circle(unsigned int dat,unsigned char type);//正反转1圈

void motor_run_counter_ahalf(unsigned int dat,unsigned char type);//180度
void motor_run_custom(unsigned int dat,unsigned char type);//自定义正反转步数

void motor_stop();//停止

#endif

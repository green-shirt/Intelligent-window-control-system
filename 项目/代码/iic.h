
#ifndef _IIC_H_
#define _IIC_H_
#include "STC8.h"
#include "intrins.h"

/************外部变量参数*******************/
#define ACK  0
#define NO_ACK  1

/**************管脚配置*********************/
sbit SDA=P1^4;
sbit SCL=P1^5;

/**************底层驱动函数*****************/
void IIC_Wait();//延时等待
void IIC_Start();	//起始函数
void IIC_Stop();	//结束函数

void IIC_Send_Byte(unsigned char byte);	//发送一个字节
   /*应答：返回ACK  非应答：返回NO_ACK*/
bit IIC_Rec_ACK();	//主机接收从机应答 
unsigned char IIC_Rec_Byte();	//接收一个字节
		/*应答：ack_back=ACK  非应答：ack_back=NO_ACK*/
void IIC_Send_ACK(bit ack);	//接收数据后发送应答信号


/***************用户函数********************/


#endif




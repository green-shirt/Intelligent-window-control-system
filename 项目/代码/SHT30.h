#ifndef __SHT30_H__
#define __SHT30_H__

/************外部变量参数*******************/
//#define  SHT30_ADDR1  0X88 
//#define  SHT30_ADDR2  0X8a
#define SHT30_OK  0
#define SHT30_ERR 1

/**************底层驱动函数*****************/
void SHT30_Delay();//延时8ms  @24.000MHz
unsigned char Get_SHT30_Value(unsigned char *p);
bit SHT30_CRC(unsigned char dat1, unsigned char dat2,unsigned char crc_dat);

/***************用户函数********************/
	/****读取成功返回：SHT30_OK   读取失败返回：SHT30_ERR***********/
bit Get_Temp_and_Humi(int *t,int*h);

#endif



#include "STC8.h"
#include "iic.h"
#include "SHT30.h"
/****************************************************
* 函数功能  :SHT30延时函数，一般延时8ms  
* 输    入  :无  
* 输    出  :无 
*****************************************************/
void SHT30_Delay()		//延时8ms  @24.000MHz
{
	unsigned char i, j;
	i = 250;
	j = 87;
	do
	{
		while (--j);
	} while (--i);
}
/****************************************************
* 函数功能  :CRC函数 
* 输    入  :待校验数据dat1，dat2，校验码crc_dat  
* 输    出  :校验成功：SHT30_OK  校验失败：SHT30_ERR 
*****************************************************/
bit SHT30_CRC(unsigned char dat1, unsigned char dat2,unsigned char crc_dat)
{
	unsigned char i;  
	unsigned char crc = 0xFF; // calculated checksum
	crc ^= dat1;
	for(i=8;i>0;--i) 
	{
		if(crc & 0x80) 
			crc=(crc<<1)^0x31;
		else
			crc=(crc<<1);
	}
	crc ^= dat2;
	for(i=8;i>0;--i) 
	{
		if(crc & 0x80) 
			crc=(crc<<1)^0x31;
		else
			crc=(crc<<1);
	}
	
	if(crc==crc_dat)
		return(SHT30_OK);   //校验成功返回0
	else
		return(SHT30_ERR);   //校验失败返回1
}

/****************************************************
* 函数功能  :SHT30读取数据 
* 输    入  :读出数据存放数组指针  
* 输    出  :错误次数 err   读取成功：SHT30_OK （0）  读取失败：错误次数 err
*****************************************************/
unsigned char Get_SHT30_Value(unsigned char *p)
{
	unsigned char i,err;
	err=0;
	IIC_Start();
	IIC_Send_Byte(0x88);//写地址
	if(IIC_Rec_ACK())err++;
	IIC_Send_Byte(0x2c);//写命令0x2C
	if(IIC_Rec_ACK())err++;
	IIC_Send_Byte(0x06);////写命令,快速模式06；中速模式：0d;慢速模式：10
	if(IIC_Rec_ACK())err++;
	IIC_Stop();

	SHT30_Delay();

	IIC_Start();
	IIC_Send_Byte(0x89);//读地址
	if(IIC_Rec_ACK())err++;
	SHT30_Delay();
	
	for(i=0;i<6;i++)
	{
		p[i]=IIC_Rec_Byte();
		IIC_Send_ACK(ACK);
	}
	p[i]=IIC_Rec_Byte();
	IIC_Send_ACK(NO_ACK);
	IIC_Stop();
	
	if(SHT30_CRC(p[0],p[1],p[2])==SHT30_ERR)err++;//校验温度数据
	if(SHT30_CRC(p[3],p[4],p[5])==SHT30_ERR)err++;//校验湿度数据
	return(err);
}

/****************************************************
* 函数功能  :SHT30读取温度和湿度 
* 输    入  :温度和湿度存储变量地址 
* 输    出  :读取成功：SHT30_OK   读取失败：SHT30_ERR
*****************************************************/
bit Get_Temp_and_Humi(int *t,int*h)
{
	unsigned char dat[6];
	long temp,humi;
	if(Get_SHT30_Value(dat)==SHT30_OK)
	{
		temp=dat[0];
		temp=temp<<8;
		temp=temp|dat[1];
		temp=temp*17500;
		temp=temp>>16;
		temp=temp-4500;
		
		humi=dat[3];
		humi=humi<<8;
		humi=humi|dat[4];
		humi=humi*10000;
		humi=humi>>16;
		
		*t=temp;
		*h=humi;
		return(SHT30_OK);
	}
	else
		return(SHT30_ERR);
}



/**************END**************************/

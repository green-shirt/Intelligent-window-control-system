#ifndef __SHOW_H__
#define __SHOW_H__
#include "oled12864.h"
#include "readdata.h"
#include "motor.h"
extern void show_sreen1();//主屏幕显示
extern void show_manual_sreen(); //手动控制模式
extern void show_time_sreen(); //时间控制模式
extern void show_tandh_sreen(); //温湿度控制模式
extern void show_Light_sreen(); //光照控制模式
extern void show_clear();
extern unsigned char dat1,dat2;
#endif
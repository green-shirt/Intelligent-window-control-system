#include "delay.h"	


void Delay_1ms()		//@24.000MHz
{
	unsigned char i, j;

	_nop_();
	i = 32;
	j = 40;
	do
	{
		while (--j);
	} while (--i);
}
unsigned char Delay_ms(unsigned int dat)
{
	while(dat--)
	{
		Delay_1ms();
	}
	return 0;
}

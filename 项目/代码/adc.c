#include "adc.h"
#include "intrins.h"


unsigned int data ADC_Value[8]={0,0,0,0,0,0,0,0};

////初始化ADC通道ch 其值为0-7对应P10-P17//////////
void adc_init(unsigned char ch)
{
	
	 P1M0 &= ~(1<<ch);                                //设置P1.x为ADC口
   P1M1 |= (1<<ch);
   ADCCFG |= 0x2f;                              //设置右对齐数据格式，ADC时钟为系统时钟/2/16/3    24M时250k
   ADC_CONTR |= 0x80;                           //打开ADC电源
	
}
////获取ADC通道ch的值，ch其值为0-7对应P10-P17//////////
void get_adc(unsigned char ch)
{
	 //选取ADC转换通道
	
	ADC_CONTR &= 0xf0;//选择转换通道
	ADC_CONTR |=ch;  //选择转换通道
	
	ADC_CONTR |= 0x40;                      //启动AD转换器
	_nop_();
	_nop_();
	while (!(ADC_CONTR & 0x20));            //查询ADC完成标志
	ADC_CONTR &= ~0x20;                     //清完成标志
	ADC_Value[ch] = (ADC_RES<<8)+ADC_RESL;                           //读取ADC结果
}
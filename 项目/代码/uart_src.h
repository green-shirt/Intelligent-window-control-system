#ifndef __UART_SRC_H__
#define __UART_SRC_H__
#include "uart.h"	
#include "delay.h"	
#include "readdata.h"
#include "main.h"
extern xdata unsigned char manual_flag,timer_flag,tandh_flag,G_flag;

void uart_send_data();

void exit_model(unsigned char *dat); //退出操作格式化

void tandh_flag_exit_model();

#endif
#ifndef _ADC_H_
#define _ADC_H_
#include "STC8.h"
/**如果没包含stc头文件就要定义一下寄存器*/
//sfr     ADC_CONTR   =   0xbc;
//sfr     ADC_RES     =   0xbd;
//sfr     ADC_RESL    =   0xbe;
//sfr     ADCCFG      =   0xde;

//sfr     P1M0        =   0x92;
//sfr     P1M1        =   0x91;


/****************************************/
extern unsigned int data ADC_Value[8];
void adc_init(unsigned char ch);//初始化ADC通道ch 其值为0-7对应P10-P17//
void get_adc(unsigned char ch);//获取ADC通道ch的值，ch其值为0-7对应P10-P17ADC，转换结果存储在数组ADC_Value[ch]
#endif

#include "DS1302.h"

//---DS1302写入和读取时分秒的地址命令---//
//---秒分时日月周年 最低位读写位;-------//
unsigned char code READ_RTC_ADDR[7] = {0x81, 0x83, 0x85, 0x87, 0x89, 0x8b, 0x8d}; 
unsigned char code WRITE_RTC_ADDR[7] = {0x80, 0x82, 0x84, 0x86, 0x88, 0x8a, 0x8c};

//---DS1302时钟初始化2019年3月4日星期一12点00分00秒。---//
//---存储顺序是秒分时日月周年,存储格式是用BCD码转换后的10进制数值---//
unsigned char TIME[7] = {57, 41, 13, 9, 5, 6, 20};

/****************************************************
* 函 数 名  : DS1302_Write
* 函数功能  : 向DS1302命令（地址+数据）
* 输    入  : add,dat
* 输    出  : 无
*****************************************************/
void DS1302_Delay1us()		//@24.000MHz STC8A
{
	unsigned char i;
	_nop_();
	_nop_();
	i = 6;
	while (--i);
}
//void DS1302_Delay1us()		//@12.000MHz STC89
//{
//	_nop_();
//}

void DS1302_Write(unsigned char add, unsigned char dat)
{
    unsigned char n;
    RST = 0;
    DS1302_Delay1us();

    SCLK = 0;//先将SCLK置低电平。
    DS1302_Delay1us();
    RST = 1; //然后将RST(CE)置高电平。
    DS1302_Delay1us();

    for (n=0; n<8; n++)//开始传送八位地址命令
    {
        DSIO = add & 0x01;//数据从低位开始传送
        add >>= 1;
        SCLK = 1;//数据在上升沿时，DS1302读取数据
        DS1302_Delay1us();
        SCLK = 0;
        DS1302_Delay1us();
    }
    for (n=0; n<8; n++)//写入8位数据
    {
        DSIO = dat & 0x01;
        dat >>= 1;
        SCLK = 1;//数据在上升沿时，DS1302读取数据
        DS1302_Delay1us();
        SCLK = 0;
        DS1302_Delay1us();    
    }   
         
    RST = 0;//传送数据结束
    DS1302_Delay1us();
}

/*********************************************************
* 函 数 名         : DS1302_Read
* 函数功能         : 读取一个地址的数据
* 输    入         : add
* 输    出         : dat
*********************************************************/

unsigned char DS1302_Read(unsigned char add)
{
    unsigned char n,dat,dat1;
    RST = 0;
    DS1302_Delay1us();

    SCLK = 0;//先将SCLK置低电平。
    DS1302_Delay1us();
    RST = 1;//然后将RST(CE)置高电平。
    DS1302_Delay1us();

    for(n=0; n<8; n++)//开始传送八位地址命令
    {
        DSIO = add & 0x01;//数据从低位开始传送
        add >>= 1;
        SCLK = 1;//数据在上升沿时，DS1302读取数据
        DS1302_Delay1us();
        SCLK = 0;//DS1302下降沿时，放置数据
        DS1302_Delay1us();
    }
    DS1302_Delay1us();
    for(n=0; n<8; n++)//读取8位数据
    {
        dat1 = DSIO;//从最低位开始接收
        dat = (dat>>1) | (dat1<<7);
        SCLK = 1;
        DS1302_Delay1us();
        SCLK = 0;//DS1302下降沿时，放置数据
        DS1302_Delay1us();
    }

    RST = 0;
    DS1302_Delay1us();    //以下为DS1302复位的稳定时间,必须的。
    SCLK = 1;
    DS1302_Delay1us();
    DSIO = 0;
    DS1302_Delay1us();
    DSIO = 1;
    DS1302_Delay1us();
    return dat; 
}
/*******************************************************************************
* 函 数 名         : BCD_DEC
* 函数功能         : BCD码转十进制码
* 输    入         : 待转换数据BCD码
* 输    出         : 转换后的十进制码
*******************************************************************************/
unsigned char BCD_DEC(unsigned char dat)
{
	return( (dat>>4)*10+(dat&0x0f) );
}
/*******************************************************************************
* 函 数 名         : DEC_BCD
* 函数功能         : 十进制码转BCD码
* 输    入         : 待转换数据十进制码
* 输    出         : 转换后的BCD码
*******************************************************************************/
unsigned char DEC_BCD(unsigned char dat)
{
	return( ((dat/10)<<4) + (dat%10) );
}
/*******************************************************************************
* 函 数 名         : DS1302_Init_Time
* 函数功能         : 重置时间.
* 输    入         : 无
* 输    出         : 无
*******************************************************************************/

void DS1302_Init_Time(unsigned char *p)
{
    unsigned char n;
    DS1302_Write(0x8E,0X00);      //禁止写保护，就是关闭写保护功能
    for (n=0; n<7; n++)//写入7个字节的时钟信号：分秒时日月周年
    {
        DS1302_Write(WRITE_RTC_ADDR[n],DEC_BCD(p[n])); 
    }
    DS1302_Write(0x8E,0x80);      //打开写保护功能
}

/*******************************************************************************
* 函 数 名         : DS1302_Read_Time
* 函数功能         : 读取时钟信息
* 输    入         : 无
* 输    出         : 读取的时间存放在数组TIME[n]
*******************************************************************************/

void DS1302_Read_Time(void)
{
    unsigned char n;
    for (n=0; n<7; n++)//读取7个字节的时钟信号：秒分时日月周年
    {
        TIME[n] = BCD_DEC( DS1302_Read(READ_RTC_ADDR[n]) );
    }
        
}

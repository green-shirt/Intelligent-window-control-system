#include "show.h"
unsigned char dat1=0,dat2=0;
void show_sreen1()//主屏幕显示
{
	OLED_Show_Chinese(0,32,0);
	OLED_Show_Chinese(0,48,1);
	OLED_Show_Chinese(0,64,2);
	OLED_Show_Chinese(0,80,3);

}

void show_manual_sreen() //手动控制模式
{
	OLED_Show_Chinese(0,16,4);
	OLED_Show_Chinese(0,32,5);
	OLED_Show_Chinese(0,48,6);
	OLED_Show_Chinese(0,64,7);	
	OLED_Show_Chinese(0,80,8);
	OLED_Show_Chinese(0,96,9);
}

void show_time_sreen() //时间控制模式
{
	OLED_Show_Chinese(0,16,10);
	OLED_Show_Chinese(0,32,11);
	OLED_Show_Chinese(0,48,6);
	OLED_Show_Chinese(0,64,7);	
	OLED_Show_Chinese(0,80,8);
	OLED_Show_Chinese(0,96,9);
}

void show_tandh_sreen() //温湿度控制模式
{
	dat1=T_int;
	OLED_Show_Chinese(0,8,12);
	OLED_Show_Chinese(0,24,13);
	OLED_Show_Chinese(0,40,14);
	OLED_Show_Chinese(0,56,6);	
	OLED_Show_Chinese(0,72,7);
	OLED_Show_Chinese(0,88,8);
	OLED_Show_Chinese(0,104,9);
	OLED_Show_String(2,0,"Temp:");
	OLED_Show_Num(2,40,dat1,2);
	if(dat1!=dat2)
	{
	dat2=dat1;
	OLED_Show_String(2,0,"Temp:");
	OLED_Show_Num(2,40,dat1,2);
	}

	OLED_Show_String(2,56,"Humi:");
	OLED_Show_Num(2,96,H_int,2);
}

void show_Light_sreen() //光照控制模式
{
	OLED_Show_Chinese(0,16,15);
	OLED_Show_Chinese(0,32,16);
	OLED_Show_Chinese(0,48,6);
	OLED_Show_Chinese(0,64,7);	
	OLED_Show_Chinese(0,80,8);
	OLED_Show_Chinese(0,96,9);
	OLED_Show_String(2,0,"Light:");
	OLED_Show_Num(2,48,G,3);
}

void show_clear()
{
	OLED_Show_String(2,40,"  ");
	OLED_Show_String(2,96,"    ");
	OLED_Show_String(4,0,"                 ");	
	OLED_Show_String(6,0,"                 ");	
}

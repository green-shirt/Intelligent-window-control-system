#ifndef _MAIN_H_
#define _MAIN_H_		
#include "uart.h"	
#include "STC8.h"	
#include "SHT30.h"
#include "key.h"
#include "oled12864.h"
#include "DS1302.h"
#include "adc.h"
#include "motor.h"
#include "delay.h"
#include "uart_src.h"	
#include "readdata.h"
#include "show.h"


extern xdata unsigned char Switch_screen_flag;

#endif
#ifndef __DS1302_H_
#define __DS1302_H_

//---包含头文件---//
#include "STC8.h"
#include <intrins.h>

/*****************DS1302管脚配置********************/
sbit DSIO=P2^2;
sbit RST=P2^1;
sbit SCLK=P2^3;

/*****************全局变量声明********************/

extern unsigned char TIME[7];   //存放读取的时间,存储顺序是秒分时日月周年
#define SECOND_NUM   0
#define MINUTE_NUM   1
#define HOUR_NUM  2
#define DAY_NUM   3
#define MONTH_NUM 4
#define WEEK_NUM  5
#define YEAR_NUM  6
/*****************底层驱动函数********************/
void DS1302_Write(unsigned char add, unsigned char dat);
unsigned char DS1302_Read(unsigned char add);
unsigned char BCD_DEC(unsigned char dat);
unsigned char DEC_BCD(unsigned char dat);

/*****************基础应用函数********************/
void DS1302_Init_Time(unsigned char *p);//重置时间
void DS1302_Read_Time(void);//读取时间



#endif

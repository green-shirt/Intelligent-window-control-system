		 
#include "uart.h"			

	

u8 receiveData[16];			
u8 usart_receive_flag=0;	
u16 i=0;
u8 temp=0;

//串口初始化
void UsartInit()
{
	SCON = 0x50;		
	AUXR |= 0x40;		
	AUXR &= 0xFE;		
	TMOD &= 0x0F;		
	TL1 = 65536-(11059200/9600/4);		
	TH1 = 65536-(11059200/9600/4)>>8;		
	ET1 = 0;		
	TR1 = 1;	
	ES = 1;
	EA = 1;
	TI=1;
}
//printf重定向
char putchar (char c)
{
  while (!TI);
  TI = 0;
  return (SBUF = c);
}
//scanf重定向
char _getkey(void)
{
    char ch;
	while(!RI); 
    ch = SBUF;
    RI=0; 
	return ch; 
}
//发送1个字节
void SendChar(char Char)
{
    SBUF=Char;
		while(!TI);
		TI=0;
}

//发送1个字符串
void SendString(char *p)
{
    while(*p!='\0')
    {
			SendChar(*p);
			p++;
    }
}

void Usart() interrupt 4
{

		if(RI)										
		{
			RI = 0;
			temp=SBUF;
				if(temp!='\n')
					{
						receiveData[i++]=SBUF;	
					}else{
						i=0;
						usart_receive_flag=1;
					}
				
		}
}

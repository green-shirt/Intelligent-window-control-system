#include "uart_src.h"	
xdata unsigned char manual_flag=0,timer_flag=0,tandh_flag=0,G_flag=0;
void uart_send_data() //串口发送接受指令
{
	/*串口控制部分*/
	if(usart_receive_flag==1&&(strncmp("model1",receiveData,6)==0)) //模式一
	{
		printf("手动控制已启动\r\n");
		manual_flag=1;
		usart_receive_flag=0;
		receiveData[0]='\0';
	}
	if(usart_receive_flag==1&&(strncmp("model2",receiveData,6)==0)) //模式二
	{
		printf("时间控制已启动\r\n");
		timer_flag=1;
		usart_receive_flag=0;
		receiveData[0]='\0';
	}
	if(usart_receive_flag==1&&(strncmp("model3",receiveData,6)==0)) //模式三
	{
		printf("光照控制已启动\r\n");
		G_flag=1;
		usart_receive_flag=0;
		receiveData[0]='\0';
	}
	if(usart_receive_flag==1&&(strncmp("model4",receiveData,6)==0)) //模式四
	{
		printf("温湿度控制已启动\r\n");
		tandh_flag=1;
		usart_receive_flag=0;
		receiveData[0]='\0';
	}
	
	/*退出控制*/
exit_model(&manual_flag); //退出手动控制
exit_model(&timer_flag); //退出时间控制
exit_model(&tandh_flag); //退出温湿度控制
exit_model(&G_flag); //退出光照控制
	

	/*标志位控制部分-2*/
switch(manual_flag) //手动控制
{
	case 1:	Delay_ms(10);
					printf("按键1：打开窗户窗帘\r\n");
					printf("按键2：关闭窗户窗帘\r\n");
					printf("按键3：电机停止 时间复位\r\n");
					printf("按键4：切换屏幕\r\n");
					Delay_ms(30);
					printf("是否退出：\r\n");
					printf("是：输入退出\r\n");
					printf("否：不做任何操作\r\n");
					manual_flag=3;
					OLED_Clear();
					Switch_screen_flag=1;
					show_manual_sreen();
					timer_flag=0;
					tandh_flag=0;
					G_flag=0;
					break;
	case 2:	manual_flag=0;
					break;
}
switch(timer_flag) //时间控制
{
	case 1:	Delay_ms(10);
					printf("已进入时间控制\r\n");	
					Delay_ms(30);
					printf("是否退出：\r\n");
					printf("是：输入退出\r\n");
					printf("否：不做任何操作\r\n");
					timer_flag=3;
					OLED_Clear();
					Switch_screen_flag=2;
					show_time_sreen();
					tandh_flag=0;
					G_flag=0;
					manual_flag=0;
					break;
	case 2:	timer_flag=0;
					break;
}
switch(tandh_flag) //温湿度控制
{
	case 1:	Delay_ms(10);
					printf("当前温度:%d，当前湿度:%d\r\n",T_int,H_int);
					Delay_ms(30);
					printf("是否退出：\r\n");
					printf("是：输入退出\r\n");
					printf("否：不做任何操作\r\n");
					tandh_flag=3;

					G_flag=0;
					manual_flag=0;
					timer_flag=0;
					break;
	case 2:	tandh_flag=0;
					break;
}
switch(G_flag) //光照控制
{
	case 1:	Delay_ms(10);
					printf("当前光照强度：%d\r\n",G);
					Delay_ms(30);
					printf("是否退出：\r\n");
					printf("是：输入退出\r\n");
					printf("否：不做任何操作\r\n");
					G_flag=3;
					OLED_Clear();
					Switch_screen_flag=3;
					show_Light_sreen();
					tandh_flag=0;
					manual_flag=0;
					timer_flag=0;
					break;
	case 2:	G_flag=0;
					break;
}
}



/*退出操作*/

void exit_model(unsigned char *dat)
{
		if(*dat==3)  //统一设置退出标志位为3
	{
	if(usart_receive_flag==1&&(strncmp("退出",receiveData,4)==0)) //模式四
	{
		Delay_ms(10);
		printf("已完成退出\r\n");
		*dat=2;
		OLED_Clear();
		Switch_screen_flag=0;
		show_sreen1();
		usart_receive_flag=0;
		receiveData[0]='\0';
	}
	}
}

//void tandh_flag_exit_model()
//{
//		if(tandh_flag==3)  //统一设置退出标志位为3
//	{
//	if(usart_receive_flag==1&&(strncmp("退出",receiveData,4)==0)) //模式四
//	{
//		Delay_ms(10);
//		printf("已完成退出\r\n");
//		tandh_flag=2;
//		OLED_Clear();
//		Switch_screen_flag=0;
//		show_sreen1();
//		usart_receive_flag=0;
//		receiveData[0]='\0';
//	}
//					show_clear();
//					Switch_screen_flag=4;
//					show_tandh_sreen();
//	}
//}


#ifndef __READDATA_H__
#define __READDATA_H__

#include "adc.h"
#include "SHT30.h"
#include "oled12864.h"
extern xdata unsigned int T,H,T_int,H_int,G;

void TempAndHumi_data();
void photos_data();
#endif
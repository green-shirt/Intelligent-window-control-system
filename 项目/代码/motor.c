#include "motor.h"
unsigned int run_num=0;

	
void Delay5ms()		//@24.000MHz
{
	unsigned char i, j;

	_nop_();
	_nop_();
	i = 156;
	j = 213;
	do
	{
		while (--j);
	} while (--i);
}
void Delay1ms()		//@24.000MHz
{
	unsigned char i, j;

	_nop_();
	i = 32;
	j = 40;
	do
	{
		while (--j);
	} while (--i);
}

void Delay500us()		//@24.000MHz
{
	unsigned char i, j;

	i = 16;
	j = 147;
	do
	{
		while (--j);
	} while (--i);
}



void motor_run_clockwise() //正转
{
	A_PH=1;B_PH=0;C_PH=0;D_PH=0;
	Delay500us();
	A_PH=0;B_PH=1;C_PH=0;D_PH=0;
	Delay500us();
	A_PH=0;B_PH=0;C_PH=1;D_PH=0;
	Delay500us();
	A_PH=0;B_PH=0;C_PH=0;D_PH=1;
	Delay500us();
}


void motor_run_counterclockwise() //反转
{
	A_PH=0;B_PH=0;C_PH=0;D_PH=1;
	Delay500us();
	A_PH=0;B_PH=0;C_PH=1;D_PH=0;
	Delay500us();
	A_PH=0;B_PH=1;C_PH=0;D_PH=0;
	Delay500us();
	A_PH=1;B_PH=0;C_PH=0;D_PH=0;
	Delay500us();
}

void motor_run_counter_circle(unsigned int dat,unsigned char type) //正反转1圈
{
	unsigned int r=512,i=0;
	i=run_num;
	if(type==Back)
	{
		while(dat--)
		{
			while(r--)
			{
				i--;
				motor_run_counterclockwise();
				if((Key_scan()==3))
				{
					break;
				}
			}
		}
		run_num=i;
		Delay_ms(10);
		printf("当前电机已后退:%d\r\n",run_num);
	}
	else
	{
		while(dat--)
		{
			while(r--)
			{
				i++;
				motor_run_clockwise();
				if(Key_scan()==3)
				{
					break;
				}
			}
		}
		run_num=i;
		Delay_ms(10);
		printf("当前电机已前进:%d\r\n",run_num);	
	}
}

void motor_run_counter_ahalf(unsigned int dat,unsigned char type) //180度次数控制
{
	unsigned int r=256,i=0;
	i=run_num;
	if(type==Back)
	{
		while(dat--)
		{
			while(r--)
			{
				i--;
				motor_run_counterclockwise();
				if((Key_scan()==3))
				{
					break;
				}
			}
		}
		run_num=i;
		Delay_ms(10);
		printf("当前电机已后退:%d\r\n",run_num);
	}
	else
	{
		while(dat--)
		{
			while(r--)
			{
				
				motor_run_clockwise();
				i++;
				if(Key_scan()==3)
				{
					break;
				}
			}
		}
		run_num=i;
		Delay_ms(10);
		printf("当前电机已前进:%d\r\n",run_num);		
	}
}


void motor_run_custom(unsigned int dat,unsigned char type) //自定义正反转步数
{
	unsigned int i=0;
	i=run_num;
	if(type==1)
	{
		while(dat--)
		{
			motor_run_clockwise();//正转
			i++;
			if(Key_scan()==3)
				{
					break;
				}
		}
		run_num=i;
		Delay_ms(10);
		printf("当前电机已前进:%d步\r\n",run_num);
	}
	else
	{
		while(dat--)
		{
			motor_run_counterclockwise();//反转
			i--;
			if(Key_scan()==3)
				{
					break;
				}
		}
		run_num=i;
		Delay_ms(10);
		printf("当前电机已后退:%d\r\n",run_num);
	}
}

void motor_stop() //停止
{
	A_PH=0;B_PH=0;C_PH=0;D_PH=0;
}



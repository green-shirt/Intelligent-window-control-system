#include "main.h"

xdata unsigned char init_time[7]={57, 44, 14, 27, 5, 6, 22}; //初始化时间
xdata unsigned int time_s,time_m,time_h,r=0;
xdata unsigned char key_flag=0,tandh_motor_flag=1,timer_control_flag=1,G_control_flag=1;
xdata unsigned char Switch_screen_flag=0;
extern xdata unsigned char Switch_screen_flag;

void uart_send_data(); //串口收发
void now_times(); //获取当前时间
void photos_data(); //获取当前光敏电压
void motor_src(); //电机控制
void TempAndHumi_data();//温湿度显示
void key_control(); //按键控制
void tempAndHuim_cont();//温湿度控制
void tandh_form(); //温湿度模块化,用于调用
void TimerControl();//时间控制
void timer_cont_form(unsigned int run_dat,unsigned char run_state);//时间控制模块化，用于调用
void timer_cont_form_2(unsigned int run_dat,unsigned char run_state);//时间控制模块化，用于调用
void G_cont_form(unsigned int run_dat,unsigned char run_state); //光敏控制模块化，用于调用
void G_cont_form_2(unsigned int run_dat,unsigned char run_state); //光敏控制模块化，用于调用
void PhotosControl();//光照控制
void G_T_H_sreen();
void main(void)
{
	UsartInit();
	OLED_Init();
	OLED_Clear();
	DS1302_Read_Time();
	show_sreen1();//屏幕显示
	while(1)
	{	
		
		key_control(); //按键控制
		now_times(); //获取当前世间
		G_T_H_sreen();
		uart_send_data();//串口收发
		photos_data();//获取当前光敏电压并转化为百分比
		TempAndHumi_data();//获取当温湿度
		motor_src();//电机控制
		tempAndHuim_cont();//温湿度控制
		TimerControl();//时间控制
		PhotosControl();//光照控制
	}
}


void now_times()
{		
	if((Switch_screen_flag==0)||(Switch_screen_flag==2))
	{
		DS1302_Read_Time();
		OLED_Show_String(2,0,"Time:");
		OLED_Show_Num(2,40,TIME[2],2);
		OLED_Show_String(2,56,":");
		OLED_Show_Num(2,74,TIME[1],2);
		OLED_Show_String(2,90,":");
		OLED_Show_Num(2,98,TIME[0],2);
	}
}
void G_T_H_sreen()
{
	if(Switch_screen_flag==0)
		{
			OLED_Show_String(4,0,"Temp:");
			OLED_Show_Num(4,40,T_int,2);
			OLED_Show_String(4,56,"Humi:");
			OLED_Show_Num(4,96,H_int,2);
			OLED_Show_String(6,0,"Light:");
			OLED_Show_String(6,88,"   ");
			OLED_Show_Num(6,48,G,3);
			OLED_Show_String(6,72,"M:");
			OLED_Show_String(6,88,"    ");
			OLED_Show_Num(6,88,run_num,3);
		}
}
void motor_src()//电机控制
{
if((manual_flag==3)||(Switch_screen_flag==1))
{
	switch(key_flag)
	{
		/*
		tandh_motor_flag:温湿度控制电机标志位
		key_flag:按键标志位
		*/
		case 1:	printf("开窗中···\r\n");
						OLED_Show_String(2,0,"             ");
						OLED_Show_String(2,0,"In operation");
						motor_run_counter_circle(1,Forward);
						printf("已完成开窗\r\n");
						printf("窗帘拉起中···\r\n");
						motor_run_custom(512,1);
						printf("已完成拉起\r\n");
						printf("恢复操作\r\n");
						OLED_Show_String(2,0,"             ");
						OLED_Show_String(2,0,"Res operation");
						OLED_Show_String(4,0,"M:");
						OLED_Show_String(4,16,"    ");
						OLED_Show_Num(4,16,run_num,3);
						timer_control_flag=0;
						tandh_motor_flag=0;
						G_control_flag=0;
						key_flag=0;
						
						break;
		case 2: printf("关窗中···\r\n");
						OLED_Show_String(2,0,"             ");
						OLED_Show_String(2,0,"Is closing");
						motor_run_counter_circle(1,Back);
						printf("已完成关窗\r\n");
						printf("窗帘关闭中···\r\n");
						motor_run_custom(512,0);
						printf("已完成关闭\r\n");
						printf("恢复操作\r\n");
						OLED_Show_String(2,0,"               ");
						OLED_Show_String(2,0,"Complete closure");
						OLED_Show_String(4,0,"M:");
						OLED_Show_String(4,16,"     ");
						OLED_Show_Num(4,16,run_num,4);
						timer_control_flag=1;
						tandh_motor_flag=1;
						G_control_flag=1;
						key_flag=0;break;
		default:key_flag=0;break;
			
	}
}
else 
	key_flag=0;//在手动控制未启动之前按键无法控制
}

void key_control() //按键控制
{
	/*按键4不参与控制电机屏幕等
		按键3停止电机运转
	*/
			switch(Key_scan()) 
		{
			case 1: key_flag=1;break; //切屏 开电机
			case 2: key_flag=2;break;
			case 3: DS1302_Init_Time(init_time);break;
			case 4:	Switch_screen_flag++;
							OLED_Clear();
							switch(Switch_screen_flag)
								{
									case 0:show_sreen1();now_times();break;
									case 1:show_manual_sreen();OLED_Show_String(2,0,"No operation");tandh_flag=0;break;
									case 2:show_time_sreen();OLED_Show_String(4,0,"No operation");tandh_flag=0;break;
									case 3:show_Light_sreen();OLED_Show_String(4,0,"No operation");tandh_flag=0;break;
									case 4:show_tandh_sreen();OLED_Show_String(4,0,"No operation");tandh_flag=0;break;
									default:Switch_screen_flag=0;show_sreen1();now_times();tandh_flag=0;break;
								}
							break;
		}
	
}

void tempAndHuim_cont()//温度控制
{
	/*大于30打开，小于27关
		温度为：22~26度 湿度为40%~70%窗户 窗户全部打开
		假设电机顺时针旋转1/2圈为打开
	*/
	if((tandh_flag==3)||(Switch_screen_flag==4)) //在温湿度模式启动
	{
		
			tandh_form();//温湿度格式化,用于调用
			show_tandh_sreen();
	}
	
}

void TimerControl()//时间控制
{
	if((timer_flag==3)||(Switch_screen_flag==2))//时间控制
	{
		/*
		优先时间控制拉开窗帘
		再对温湿度判断是否打开窗户
		0为关闭
		1为拉起
		*/
		
		if(TIME[2]==8&&TIME[1]==0&&TIME[0]==0)//早上8点拉起
		{
			timer_cont_form(512,1);
		}
		if(TIME[2]==12)
			{
				timer_cont_form_2(512,0);
			}
		if(TIME[2]==14&&TIME[1]==20&&TIME[0]==0)
			{
				timer_cont_form(522,1);
			}
		if(TIME[2]==19&&TIME[1]==0&&TIME[0]==0)
			{
				timer_cont_form(522,0);
			}
	}
}

void PhotosControl()//光照控制
{
	if((G_flag==3)||(Switch_screen_flag==3))//光照控制
	{
		show_Light_sreen();
		if(G>=20&&G<30)
		{
			G_cont_form(512,1);//拉起窗帘 100%
		}
		if(G>=0&&G<10)
		{
			G_cont_form(512,0);//关闭窗帘 100%
		}
		if(G>=80)
		{
			G_cont_form_2(512,0);//关闭窗帘 100%
		}
		if(G>=60&&G<80)
		{
			G_cont_form(512,0);//关闭窗帘 50%
		}
		
	}
}

void tandh_form()//温湿度模块化,用于调用
{
		if((T_int<20&&T_int>35)&&(H_int<70&&H_int>30))
		{
		if(tandh_motor_flag==1) //只有第一次进入这个区间电机才会启动，避免重复循环
		{
			OLED_Show_String(4,0,"              ");
			OLED_Show_String(4,0,"Open window");
			printf("当前温湿度比较合适开窗···\r\n");
			Delay_ms(10);
			printf("窗户打开中···\r\n");
			printf("请勿进行其他操作\r\n");
			motor_run_counter_ahalf(1,Forward);
			printf("已完成打开\r\n");
			tandh_motor_flag=0;
			OLED_Show_String(4,0,"              ");
			OLED_Show_String(4,0,"Res operation");
			OLED_Show_String(6,0,"M:");
			OLED_Show_String(6,16,"     ");
			OLED_Show_Num(6,16,run_num,4);
		}
		}
		else
		{
		if(tandh_motor_flag==0) //判断窗户是否为打开状态
		{
			OLED_Show_String(4,0,"              ");
			OLED_Show_String(4,0,"Close window");
			printf("当前温湿度不适宜开窗···\r\n");
			Delay_ms(10);
			printf("窗户关闭中···\r\n");
			printf("请勿进行其他操作\r\n");
			motor_run_counter_ahalf(1,Back);
			printf("已完成关闭\r\n");
			tandh_motor_flag=1;
			OLED_Show_String(4,0,"                  ");
			OLED_Show_String(4,0,"Complete closure");
			OLED_Show_String(6,0,"M:");
			OLED_Show_String(6,16,"     ");
			OLED_Show_Num(6,16,run_num,4);
		}
		}
}

void timer_cont_form(unsigned int run_dat,unsigned char run_state) //时间控制模块化，用于调用
{
	if(timer_control_flag==1)//仅在第一次进入打开
		{
			OLED_Show_String(4,0,"              ");
			OLED_Show_String(4,0,"In operation");
			Delay_ms(10);
			printf("窗帘拉起中···\r\n");
			printf("请勿进行其他操作\r\n");
			motor_run_custom(run_dat,run_state);
			printf("已完成拉起\r\n");
			printf("恢复操作\r\n");
			timer_control_flag=0;
			OLED_Show_String(4,0,"              ");
			OLED_Show_String(4,0,"Res operation");
			OLED_Show_String(6,0,"M:");
			OLED_Show_String(6,16,"     ");
			OLED_Show_Num(6,16,run_num,4);
			/*判定温湿度情况*/
			tandh_form();//温湿度格式化,用于调用
			
		}
}

void timer_cont_form_2(unsigned int run_dat,unsigned char run_state) //时间控制模块化，用于调用
{
	if(timer_control_flag==0)//仅在第一次进入打开
		{
			OLED_Show_String(4,0,"            ");
			OLED_Show_String(4,0,"Is closing");
			Delay_ms(10);
			printf("窗帘关闭中···\r\n");
			printf("请勿进行其他操作\r\n");
			motor_run_custom(run_dat,run_state);
			printf("已完成关闭\r\n");
			printf("恢复操作\r\n");
			timer_control_flag=1;
			/*判定温湿度情况*/
			OLED_Show_String(4,0,"                  ");
			OLED_Show_String(4,0,"Complete closure");
			OLED_Show_String(6,0,"M:");
			OLED_Show_String(6,16,"      ");
			OLED_Show_Num(6,16,run_num,4);
			tandh_form();//温湿度模块化,用于调用
		}
}

void G_cont_form(unsigned int run_dat,unsigned char run_state) //光敏控制模块化，用于调用
{
	if(G_control_flag==1&&(run_num<run_dat))//仅在第一次进入打开
		{
			OLED_Show_String(4,0,"              ");
			OLED_Show_String(4,0,"In operation");
			Delay_ms(10);
			printf("窗帘打开中···\r\n");
			printf("请勿进行其他操作\r\n");
			motor_run_custom((run_dat-run_num),run_state);
			printf("已完成关闭\r\n");
			printf("恢复操作\r\n");
			G_control_flag=0;
			/*判定温湿度情况*/
			OLED_Show_String(4,0,"              ");
			OLED_Show_String(4,0,"Res operation");
			OLED_Show_String(6,0,"M:");
			OLED_Show_String(6,16,"     ");
			OLED_Show_Num(6,16,run_num,4);
			tandh_form();//温湿度格式化,用于调用
			
		}
		else
		{
			G_control_flag=0;
		}
}
void G_cont_form_2(unsigned int run_dat,unsigned char run_state) //光敏控制格式化，用于调用
{
	if(G_control_flag==0&&(run_num>run_dat))//仅在第一次进入打开
		{
			OLED_Show_String(4,0,"            ");
			OLED_Show_String(4,0,"Is closing");
			Delay_ms(10);
			printf("窗帘关闭中···\r\n");
			printf("请勿进行其他操作\r\n");
			motor_run_custom((run_num-run_dat),run_state);
			printf("已完成关闭\r\n");
			printf("恢复操作\r\n");
			G_control_flag=1;
			/*判定温湿度情况*/
			OLED_Show_String(4,0,"            ");
			OLED_Show_String(4,0,"Is closing");
			OLED_Show_String(6,0,"M:");
			OLED_Show_String(6,16,"     ");
			OLED_Show_Num(6,16,run_num,4);
			tandh_form();//温湿度格式化,用于调用
		}
		else
		{
			G_control_flag=1;
		}
}



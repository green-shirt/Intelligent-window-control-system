#ifndef __UART_H__
#define __UART_H__
#include "stdio.h"
#include "STC8.h"			 
#include "string.h"	
#define u16 unsigned int
#define u8 unsigned char
extern void UsartInit();
extern void SendChar(char Char);
extern void SendString(char *p);

void Usart();
char _getkey(void);
extern u8 receiveData[16];			
extern u8 usart_receive_flag;	
extern u16 i;
extern u8 temp;
char putchar (char c);
#endif
